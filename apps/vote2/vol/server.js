var express = require('express');
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var path = require('path');

var app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
// Utilisation des sessions
app.use(session({secret: 'todotopsecret'}))

app.use(function(req, res, next){
  if (typeof(req.session.questionList) == 'undefined') {
    req.session.questionList = "";
  }
  next();
})



app.get('/todo', function(req, res) {
  res.render('todo.ejs', {choix2: req.session.questionList});
});

// Gestion des routes
app.get('/accueil', function(req, res) {
  res.redirect('/todo');
});

// app.post('/listVote', urlencodedParser, function(req, res) {
//   if (req.body.newtodo != '') {
//     req.session.questionlist.push(req.body.newtodo);
// }
// res.redirect('/accueil');
// });

app.get('/addVote', function(req, res) {
    res.render('addVote.ejs', {choix2: req.session.choix2});
});

app.get('/Quitter', function(req, res) {

});

// Redirection vers  l accueil pour toutes les mauvaises url demandees
// app.use(function(req, res, next){
//   res.redirect('/accueil');
// })

// app.post('/voteApp', urlencodedParser, function(req, res) {
//   if ( req.body.choix2 != '') {
//     req.session.questionList.push(req.body.choix2);
// }
// res.render('todo.ejs');
// });


app.post('/handler', function(req, res) {
  console.log(req.body);
  res.send(req.body.choix2);
});

app.listen(3000);






