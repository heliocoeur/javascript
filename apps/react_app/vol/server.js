var express = require('express');
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var app = express();

app.use(cookieParser("Signing text example"));

// routes
app.get('/', function(req, res) {
    res.sendFile(__dirname + './views/index.html');
});

app.get('/test', function(req, res) {
    res.render('test.ejs', {choix: req.session.choix1});
});

app.get('/addVote', function(req, res) {
    res.render('addVote.ejs', {choix: req.session.choix1});
});

// Chargement de socket.io
var io = require('socket.io').listen(server);

// Quand un client se connecte, on le note dans la console
io.sockets.on('connection', function (socket) {
    console.log('Un client est connecté !');
});




app.listen('3000')