var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://frederick:Ragnarok09@mongodb_vote:27017";


// Create database
MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  db.close();
});

// Create collection
MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("voteApp");
  dbo.createCollection("listVote", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

// Insert 
MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("voteApp");
  var myobj = { question: "Cinema", choix1: "Oui", choix2: "Non" };
  dbo.collection("listVote").insertOne(myobj, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
    db.close();
  });
});


// Find all
MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("voteApp");
  dbo.collection("listVote").find({}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});

// Find spec
MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("voteApp");
  dbo.collection("listVote").find({ question : {$exists: true}}, { question : 1, _id : 0 }).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});

MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("voteApp");
  dbo.collection("listVote").find({ question : {$exists: true}}, { question : 1, _id : 0 }, function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});