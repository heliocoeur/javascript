var express = require('express');
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var app = express();


/* On utilise les sessions */
app.use(session({secret: 'todotopsecret'}))

app.use(function(req, res, next){
  if (typeof(req.session.todolist) == 'undefined') {
    req.session.todolist = [];
  }
  next();
})

/* Gestion des routes en-dessous
   ....                         */
app.get('/todo', function(req, res) {
  res.render('todo.ejs', {todolist: req.session.todolist});
});

app.post('/todo/ajouter', urlencodedParser, function(req, res) {
  if (req.body.newtodo != '') {
    req.session.todolist.push(req.body.newtodo);
}
res.redirect('/todo');
});

app.get('/todo/supprimer/:id', function(req, res) {
    if (req.params.id != '') {
        req.session.todolist.splice(req.params.id, 1);
    }
    res.redirect('/todo');
});

/* On redirige vers la todolist si la page demandée n'est pas trouvée */
app.use(function(req, res, next){
  res.redirect('/todo');
})


app.listen(3000);



// const path = require('path')
// const exphbs = require('express-handlebars')
// const express = require('express')
// const app = express()


// app.get('/', function(req, res) {
//   res.setHeader('Content-Type', 'text/plain');
//   res.send('Vous êtes à l\'accueil');
// });

// app.listen(3000);

// app.get('/', (request, response) => {
//   response.send('Hello from Express!')
// })





// const mongo = require('mongodb');
// const MongoClient = mongo.MongoClient;

// const url = 'mongodb://localhost:27017';

// MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {

//     if (err) throw err;

//     console.log(client.topology.clientInfo);

//     client.close();
// });






