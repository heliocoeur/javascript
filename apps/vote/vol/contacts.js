let tabContacts = [];
tabContacts.push({nom: "Levisse", prenom: "Carole"});
tabContacts.push({nom: "Rochemont", prenom: "Denise"});



function printChoice() {
    console.log("Bienvenue dans le gestionnaire de votes !");
    console.log("1 : Lister les votes en cours");
    console.log("2 : Ajouter un vote");
    console.log("3 : Voter");
    console.log("0 : Quitter");
    let choice = prompt("Choisissez une option ");
    switch(choice) {
        case '1': 
            listVote();
            break
    
        case '2': 
            createVote();
            break

        case '3': 
            addVote();
            break

        case '0': 
            return;
            break
    
        default: 
            printChoice();
            break
    };
};


function printContacts() {
    console.log("Voici la liste de tous vos contacts :")
    for  (var i = 0; i < tabContacts.length; i++) {
        console.log(`nom: ${tabContacts[i].nom}, prenom: ${tabContacts[i].prenom}`);
    }
}

function addContact() {
    let namenew = prompt("Entrez le nom ?");
    let prenomnew = prompt("Entrez le prenom ?");

    tabContacts.push({ nom: namenew, prenom: prenomnew});
    console.log(`Le contact ${namenew}, ${prenomnew} a été ajouté`);
};

printChoice();